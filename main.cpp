/***
 * Rover 2.0 Project
 * @author Ian Vos
 ***/

#ifdef __arm__ 
#include <wiringPi.h>
#else
#include "wiringPlacement/wiringTest.h"
#endif

#include <iostream>
#include <unistd.h>

#include "socketServer/socketServer.hpp"

enum wire {
    BLUE = 7, 
    GREEN = 0, 
    YELLOW = 2,
    ORANGE = 3 
};

 // TODO: Add startup code

void leftForward(){
  digitalWrite (YELLOW, 1);
  digitalWrite (ORANGE,  0);
}
void leftBackwards(){
  digitalWrite (ORANGE, 1);
  digitalWrite (YELLOW,  0);
}
void leftStop(){
  digitalWrite (ORANGE, 0);
  digitalWrite (YELLOW,  0);
}
void rightForward(){
  digitalWrite (BLUE, 1);
  digitalWrite (GREEN,  0);
}
void rightBackwards(){
  digitalWrite (GREEN, 1);
  digitalWrite (BLUE,  0);
}
void rightStop(){
  digitalWrite (GREEN, 0);
  digitalWrite (BLUE,  0);
}



 /***
  * 
  * 
  ***/
int main(){
  wiringPiSetup();
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(ORANGE, OUTPUT);

  SocketServer server;
  

  server.startServer();
  bool clientConnected = false;
  while(true){

    if(clientConnected) {
      int msg;
      msg = server.listenToClient();
      switch(msg){
        case -1: 
          leftStop();
          rightStop();
          break;
        case 0: 
          leftForward();
          rightForward();
          break;
        case 1:  
          leftBackwards();
          rightBackwards();
          break;
        case 2: 
          leftStop();
          rightForward();
          break;
        case 3: 
          leftForward();
          rightStop();
          break;

        case 999:
          std::cout << "Stopping server" << '\n';
          leftStop();
          rightStop();
          server.closeSocket();
          clientConnected = false;
          break;
      }
    } 
    else {
      /* Starting to listen to a new client */
      sleep(1);
      std::cout << "waiting..." << '\n';
      server.acceptClient();
      std::cout << "accepted..." << '\n';
      clientConnected = true;
    }
  }



  std::cin.get();
  return 0;
}

 // TODO: ADD test method

 // TODO: Add driving functionality

 // TODO: Add remote controll functionality