#include <iostream>
#include <string>

void digitalWrite(int pin, int state){
    std::cout << "Digital write: " << pin << " to " << state << '\n';
}

void wiringPiSetup(){
    std::cout << "Setup complete" << '\n';
}

enum pintModus{
    OUTPUT = 0,
    INPUT = 1
};

void pinMode(int pin, pintModus modus){
    std::cout << "pin modus of pin: " << pin << " set to " << modus << '\n';
}
